# Self-serving reader Web app

In this project, I design, architect and implement the web app solution basing on AWS services. This web app will help me preview data stored on AWS (for example, file in S3, or table in RDS), without requiring me login to access.

## The Architect

I refer to AWS web app architect and its best practices to build this solution: [Link](https://docs.aws.amazon.com/whitepapers/latest/web-application-hosting-best-practices/an-aws-cloud-architecture-for-web-hosting.html)
![architect](/image/system-architect-AWS.png)

The AWS services I used to build the solution are:

- VPC for network infrastructure
- EC2 server that run docker container to server requests. For high availability, I put these servers in an Auto Scaling Group.
- RDS database for data persistence.
- Redis for data caching.
- Application Load Balancer to load balance trafict betweens web servers.
- Route 53 for custom domain
- Certificate Manager for managing Load Balancer's TLS certificate
- CloudWatch agent to collect system metrics

## About IaC

Most of the infrastructure are provisioned using Terraform. The Terraform source files are under **/infra** directory

## About CI/CD

To implement The CI/CD pipeline to deploy the application on AWS, I use Jenkins. The pipeline is defined in Jenkinsfile. This pipeline will be trigger when I commit code to main branch of this repo
