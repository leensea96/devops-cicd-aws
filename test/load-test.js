import http from 'k6/http';

export default function () {
  const url = '%endpoint-url%';
  const payload = JSON.stringify({
    objectName: '%object%',
  });

  const params = {
    headers: {
      'Content-Type': 'application/json',
    },
  };

  http.post(url, payload, params);
}