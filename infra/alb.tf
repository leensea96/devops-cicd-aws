module "alb" {
  source  = "terraform-aws-modules/alb/aws"
  version = "~> 8.0"

  name = "web-app-alb"

  load_balancer_type = "application"

  vpc_id          = module.vpc.vpc_id
  subnets         = module.vpc.public_subnets
  security_groups = [aws_security_group.alb_sg.id]


  target_groups = [
    {
      name_prefix      = "tg-"
      backend_protocol = "HTTP"
      backend_port     = 8000
      target_type      = "instance"
      targets = {
        my_target_1 = {
          target_id = aws_instance.web_server["ap-southeast-1a"].id
          port      = 8000
        }
        my_target_2 = {
          target_id = aws_instance.web_server["ap-southeast-1b"].id
          port      = 8000
        }
      }
    }
  ]

  https_listeners = [
    {
      port               = 443
      protocol           = "HTTPS"
      certificate_arn    = "arn:aws:acm:ap-southeast-1:520812544446:certificate/b8e5089d-0949-4890-83ad-7b3cb0e915b7"
      target_group_index = 0
    }
  ]
}
