variable "region" {
  type    = string
  default = "ap-southeast-1"
}

variable "private_subnets_cidr" {
  type    = list(any)
  default = ["10.0.1.0/24", "10.0.2.0/24"]
}

variable "public_subnets_cidr" {
  default = ["10.0.101.0/24", "10.0.102.0/24"]
  type    = list(any)
}

variable "db_subnets_cidr" {
  default = ["10.0.201.0/24", "10.0.202.0/24"]
  type    = list(any)
}