terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "5.15.0"
    }
  }
}

provider "aws" {
  # Configuration options
  region = "ap-southeast-1"
}

resource "aws_instance" "web_server" {
  ami                         = "ami-0df7a207adb9748c7"
  instance_type               = "t2.medium"
  associate_public_ip_address = true
  key_name                    = "key-mac"
  ebs_block_device {
    device_name = "/dev/sdf"
    encrypted   = true
    volume_size = 30
    volume_type = "standard"
  }
  subnet_id              = "subnet-01bc07d37eba41724"
  vpc_security_group_ids = ["sg-086206a300a8c4723"]
  tags = {
    Name = "DevOps Server"
  }
  user_data = file("user_data.txt")
}