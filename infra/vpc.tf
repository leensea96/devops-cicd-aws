# Use module to create VPC
module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "sap-tf-web-vpc"
  cidr = "10.0.0.0/16"

  azs              = ["ap-southeast-1a", "ap-southeast-1b"]
  private_subnets  = var.private_subnets_cidr
  public_subnets   = var.public_subnets_cidr
  database_subnets = var.db_subnets_cidr

  enable_nat_gateway     = true
  single_nat_gateway     = true
  one_nat_gateway_per_az = false

  create_database_subnet_group = true

  tags = {
    purpose   = "sap"
    create_by = "module"
  }
}

resource "aws_security_group" "web_server_sg" {
  name   = "sap-web-server-sg"
  vpc_id = module.vpc.vpc_id
  ingress {
    description = "SSH from public internet"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "HTTP from public internet"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "defined port access from public internet"
    from_port   = 8000
    to_port     = 8000
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "Web Server Security Group"
  }
}

resource "aws_security_group" "db_sg" {
  name   = "sap-db-sg"
  vpc_id = module.vpc.vpc_id
  ingress {
    description = "SSH from public internet"
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = module.vpc.public_subnets_cidr_blocks
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "Database Security Group"
  }
}

resource "aws_security_group" "alb_sg" {
  name   = "app-load-balancer-sg"
  vpc_id = module.vpc.vpc_id

  ingress {
    description = "HTTP from public internet"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    description = "defined port access from public internet"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  tags = {
    Name = "ALB Security Group"
  }
}
