resource "aws_instance" "web_server" {
  for_each                    = toset(["ap-southeast-1a", "ap-southeast-1b"])
  availability_zone           = each.value
  ami                         = "ami-0df7a207adb9748c7"
  instance_type               = "t2.small"
  associate_public_ip_address = true
  key_name                    = "key-mac"
  iam_instance_profile        = "sap-iam-role"
  ebs_block_device {
    device_name = "/dev/sdf"
    encrypted   = true
    volume_size = 20
    volume_type = "standard"
  }
  subnet_id              = element(module.vpc.public_subnets, index(["ap-southeast-1a", "ap-southeast-1b"], each.value))
  vpc_security_group_ids = [aws_security_group.web_server_sg.id]
  tags = {
    Name = "sap-web-server-${each.key}"
  }
  user_data = replace(replace(file("user_data.txt"), "%HOST_NAME%", module.db.db_instance_address), "%PASSWORD%", data.aws_secretsmanager_secret_version.db_password.secret_string)
}
