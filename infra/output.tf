output "db_address" {
  value = module.db.db_instance_address
}

output "alb_endpoint" {
  value = module.alb.lb_dns_name
}