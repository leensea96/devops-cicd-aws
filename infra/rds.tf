module "db" {
  source = "terraform-aws-modules/rds/aws"

  identifier = "app-db"

  engine               = "mysql"
  engine_version       = "8.0"
  family               = "mysql8.0"
  major_engine_version = "8.0"
  instance_class       = "db.t3.micro"
  allocated_storage    = 10

  db_name  = "database1"
  username = "admin"
  # password = "iambawmim"
  port = "3306"

  iam_database_authentication_enabled = false

  vpc_security_group_ids = [aws_security_group.db_sg.id]

  maintenance_window = "Mon:00:00-Mon:03:00"
  backup_window      = "03:00-06:00"

  # Enhanced Monitoring - see example for details on how to create the role
  # by yourself, in case you don't want to create it automatically
  monitoring_interval    = "30"
  monitoring_role_name   = "sap-RDSMonitoringRole"
  create_monitoring_role = true

  skip_final_snapshot = true

  tags = {
    Owner       = "lehai"
    Environment = "prod"
    Purpose     = "sap"
  }

  # DB subnet group
  db_subnet_group_name = module.vpc.database_subnet_group
  # create_db_subnet_group = true
  # subnet_ids             = module.vpc.database_subnets_cidr_blocks

  # Database Deletion Protection
  deletion_protection = false

  parameters = [
    {
      name  = "character_set_client"
      value = "utf8mb4"
    },
    {
      name  = "character_set_server"
      value = "utf8mb4"
    }
  ]

  options = [
    {
      option_name = "MARIADB_AUDIT_PLUGIN"

      option_settings = [
        {
          name  = "SERVER_AUDIT_EVENTS"
          value = "CONNECT"
        },
        {
          name  = "SERVER_AUDIT_FILE_ROTATIONS"
          value = "37"
        },
      ]
    },
  ]
}
