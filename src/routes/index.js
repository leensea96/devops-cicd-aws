var express = require('express');
var router = express.Router();
os = require('os');
const {subscribe} = require("./lib/query-ddb.js");


router.get("/index", function (req, res, next) {
    res.redirect("index.html");
})

router.get('/healthcheck', function (req, res, next) {
    res.render('index', {version: req.app.locals.version, hostname: os.hostname()})
})

router.post('/subscribe' , function (req,res) {
    const new_email = req.body.email
    subscribe(new_email);
    res.redirect("confirm.html")
})
module.exports = router;