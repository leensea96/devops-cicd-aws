
// // Import required AWS SDK clients and commands for Node.js
// import { QueryCommand } from "@aws-sdk/client-dynamodb";
// import { dynamoClient } from "./dynamoClient.js";

// // Set the parameters
// export const params = {
//   ProjectionExpression: "email, firstName",
//   TableName: "Employees",
// };

// export const run = async () => {
//   try {
//     const data = await dynamoClient.send(new QueryCommand(params));
//     data.Items.forEach(function (element) {
//       console.log(element.email.S);
//     });
//     return data;
//   } catch (err) {
//     console.error(err);
//   }
// };
// run();

const { PutItemCommand, ScanCommand } = require("@aws-sdk/client-dynamodb");
const { dynamoClient } = require("./dynamoClient.js");
// Set the parameters.
const scan_params = {
  TableName: "Subscribers",
  ProjectionExpression: "id, email"
};


exports.subscribe = async (new_email) => {
  try {
    const data = await dynamoClient.send(new ScanCommand(scan_params));
    let id_arr = Array.from(data.Items, (x) => x.id.N);
    let email_arr = Array.from(data.Items, (x) => x.email.S);
    if (!email_arr.includes(new_email)) {
      while (true) {
        const new_id = Math.floor(Math.random() * 10000)
        if (!id_arr.includes(new_id)) {
          const write_params = {
            TableName: "Subscribers",
            Item: {
              id: { N: new_id.toString() },
              email: { S: new_email },
              date: {S: Date()}
            },
          };
          const data = await dynamoClient.send(new PutItemCommand(write_params));
          console.log("New User Subscribed!");
          break;
        }
        else {
          continue
        }
      }

    }
    else {
      console.log("User existed!") 
    }
  }
  catch (err) {
  console.log("Error", err);
}
};
// scanTable();

