// Without Redis
// var express = require('express');
// var router = express.Router();
// var mysql = require('mysql');

// /* Download page. */
// router.get('/', function (req, res, next) {
//     res.render('download', { target: "Item" });
//     //Use html file for form
//     //res.sendFile("/Users/haile/Documents/TechStack/Code/Nodejs/Web-Development/app-test/public/html/download.html");
// });

// router.post('/', function (req, res, next) {
//     const { GetObjectCommand } = require("@aws-sdk/client-s3");
//     const { s3Client } = require("./lib/s3Client.js");
//     const bucketParams = {
//         Bucket: "haile-bucket",
//         Key: req.body.objectName,
//     };

//     const showFileContent = async () => {
//         try {
//             // Get the object from the Amazon S3 bucket. It is returned as a ReadableStream.
//             const data = await s3Client.send(new GetObjectCommand(bucketParams));
//             // Convert the ReadableStream to a string.
//             //await data.Body.transformToString();
//             let content = await data.Body.transformToString();
//             //res.render('download', { fileName: req.body.fileName, content: content })
//             console.log("File sent!")
//             res.set('Content-Type', 'application/json')
//             res.send(content)
//         } catch (err) {
//             console.log("Error", err);
//             res.send("Error: " + err)
//         }
//     };
//     showFileContent();
// });

// router.get('/db', function (req, res, next) {
//     res.render('download', { target: "Database" });
//     //Use html file for form
//     //res.sendFile("/Users/haile/Documents/TechStack/Code/Nodejs/Web-Development/app-test/public/html/download.html");
// });

// router.post('/db', function (req, res, next) {
//     const connection = mysql.createConnection({
//                         host: '$HOST',
//                         user: '$USER',
//                         password: '$PASSWORD',
//                         database: '$DB'
//                     });

//     connection.connect();

//     connection.query(`SELECT * FROM ${req.body.objectName}`, function (error, results, fields) {
//         if (error) throw error;
//         //let obj = {}
//         for (item of results) {
//         console.log('Item: ', item);
//         //obj[item.id] = item.name
//         }
//         console.log("Sending response...")
//         res.send(results);
//     });

//     connection.end();
// });

// module.exports = router;




// With Redis
var express = require('express');
var router = express.Router();
var redis = require('redis');
var mysql  = require('mysql');

/* Download page. */
router.get('/', function (req, res, next) {
    res.render('download', { target: "Item" });
    //Use html file for form
    //res.sendFile("/Users/haile/Documents/TechStack/Code/Nodejs/Web-Development/app-test/public/html/download.html");
});

router.post('/', function (req, res, next) {
    const { GetObjectCommand } = require("@aws-sdk/client-s3");
    const { s3Client } = require("./lib/s3Client.js");
    const bucketParams = {
        Bucket: "haile-bucket",
        Key: req.body.objectName,
    };

    const showFileContent = async () => {
        try {
            // Get the object from the Amazon S3 bucket. It is returned as a ReadableStream.
            const data = await s3Client.send(new GetObjectCommand(bucketParams));
            // Convert the ReadableStream to a string.
            //await data.Body.transformToString();
            let content = await data.Body.transformToString();
            //res.render('download', { fileName: req.body.fileName, content: content })
            console.log("File sent!")
            res.set('Content-Type', 'application/json')
            res.send(content)
        } catch (err) {
            console.log("Error", err);
            res.send("Error: " + err)
        }
    };
    console.log(req.protocol)
    showFileContent();
});

router.get('/db', function (req, res, next) {
    res.render('download', { target: "Database" });
    //Use html file for form
    //res.sendFile("/Users/haile/Documents/TechStack/Code/Nodejs/Web-Development/app-test/public/html/download.html");
});

router.post('/db', function (req, res, next) {
    
    async function  run() {
        // Set up db_query, and use this as the key for redis data
        const db_query = `SELECT * FROM ${req.body.objectName}`
        // Set up redis connection
        const client = await redis.createClient({
            password: '$REDIS_PASSWORD',
            socket: {
                host: '$REDIS_HOST',
                port: 18051
            }
        });
        await client.connect();
        // Check if data is already cache in Redis or not
        const result =  await client.get(db_query)
        // If data not in Redis cache, query the DB to get the result
        if (!result) {
            const connection = mysql.createConnection({
                        host: '$HOST',
                        user: '$USER',
                        password: '$PASSWORD',
                        database: '$DB'
            });
            connection.connect();
            connection.query(db_query, async function (error, results, fields) {
                if (error) throw error;
                // let obj = {}
                // for (item of results) {
                // console.log(item);
                // }
                await client.set(db_query, JSON.stringify(results));
                console.log(results)
                console.log(JSON.stringify(results))
                console.log("Sending response...")
                res.send(results);
            });
            connection.end();
        }
        // If data already in Redis cache, query Redisto with db_query as key to get result
        else {
            console.log("The result is: ", JSON.parse(result))
            res.send(JSON.parse(result));
        }
    }
    run();
});

module.exports = router;
